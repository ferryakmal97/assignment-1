import React, {Component, Fragment} from 'react'
import {View, Text, StyleSheet, TouchableOpacity, Image, Dimensions} from 'react-native'
import Axios from 'axios';
import { PrimaryColor, SecondaryColor } from './ThemeColors';

class MovieLabel extends Component {
    constructor(props){
        super(props);
        this.state = {
          response: {},
          listMovie : []
        }
    }

    fetchAPI = async()=>{
        const ress = await Axios.get('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
        const resMov = ress.data
        
        this.setState ({
          response: ress,
          listMovie: resMov.Search
        })
    }

    componentDidMount(){
      this.fetchAPI()
    }

    render(){
        
        return(
            <Fragment>
                {
                    this.state.listMovie.map((isi, i) => { 
                    return (
                    <View key={i}>
                        <TouchableOpacity style={styles.labelMovie} >
                            <Image style={styles.poster} source={{uri:isi.Poster}}/>
                            <View style={styles.ed}>
                                <Text style={styles.MovieTitle}>{isi.Title}</Text>
                                <View>
                                    <Text style={styles.MovieAttributes}>{isi.Type}</Text>
                                    <Text style={styles.MovieAttributes}>{isi.Year}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    )
                    })
                }
            </Fragment>
        )
    }
}

export default MovieLabel
const {height, width} = Dimensions.get('window')
const styles = StyleSheet.create({
    labelMovie: {
        flexDirection:'row',
        backgroundColor:PrimaryColor,
        padding: 8,
        marginHorizontal: 35,
        marginBottom:10,
        borderRadius:10,
        borderColor:SecondaryColor,
        borderTopLeftRadius:40,
        borderBottomRightRadius:40,
        borderTopWidth:8,
        borderBottomWidth:8,
    },
    poster:{ 
        height: height/5, 
        width: width/5, 
        marginHorizontal:12,
        borderRadius:10
    },
    ed:{
        justifyContent:'space-between'
    },
    MovieTitle:{
        color:SecondaryColor, 
        maxWidth:210,
        fontSize:20,
        fontWeight:'bold'
    },
    MovieAttributes:{
        color:SecondaryColor,
        fontSize:15,
        fontWeight:'bold'
    }
})