import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../pages/HomeScreen';                                                               

const Stack = createStackNavigator();
const Router = () => {
    return(
        <NavigationContainer>
            <Stack.Navigator initialRouteName={HomeScreen} >
                <Stack.Screen name="HomeScreen" component={HomeScreen} options={{headerShown:false}}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Router;